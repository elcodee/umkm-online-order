import { createBrowserRouter } from "react-router-dom";
import ErrorPage from "../screen/error";
import { ClientStartScreen } from "../screen/user/start";
import { ClientSignUp } from "../screen/user/sign_up";
import { ClientSignIn } from "../screen/user/sign_in";
import { MerchantSignUp } from "../screen/merchant/sign_up";
import { MerchantSignIn } from "../screen/merchant/sign_in";
import { MerchantStartScreen } from "../screen/merchant/start";
import { StartScreen } from "../screen/start";
import { MerchantSuccess } from "../screen/merchant/success";
import { MerchantWaiting } from "../screen/merchant/waiting";
import { ClientSuccess } from "../screen/user/success";
import { ClientProfile } from "../screen/user/profile";
import { MerchantProfile } from "../screen/merchant/profile";
import { ClientHome } from "../screen/user/home";
import { MerchantHome } from "../screen/merchant/home";
import { ClientItem } from "../screen/user/items";
import { ClientAllChat } from "../screen/user/all_chat";
import { MerchantAllChat } from "../screen/merchant/all_chat";

export const mainRouter = createBrowserRouter([
    {
      path: "/",
      element: <StartScreen />,
      errorElement: <ErrorPage />,
    },


    // User Route's
    {
      path: "/client",
      element: <ClientStartScreen />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/sign-up",
      element: <ClientSignUp />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/sign-in",
      element: <ClientSignIn />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/success",
      element: <ClientSuccess />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/profile",
      element: <ClientProfile />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/home",
      element: <ClientHome />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/item",
      element: <ClientItem />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/allchat",
      element: <ClientAllChat />,
      errorElement: <ErrorPage />,
    },

    // Merchant Route's
    {
      path: "/merchant",
      element: <MerchantStartScreen />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/sign-up",
      element: <MerchantSignUp />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/sign-in",
      element: <MerchantSignIn />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/success",
      element: <MerchantSuccess />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/waiting",
      element: <MerchantWaiting />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/profile",
      element: <MerchantProfile />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/home",
      element: <MerchantHome />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/allchat",
      element: <MerchantAllChat />,
      errorElement: <ErrorPage />,
    },
  ]);