import Cards from "./content/Cards"
import Footer from "./content/Footer"
import SearchBar from "./content/SearchBar"
import { NavBar } from "./content/NavBar"

export const ClientHome = () => {
    return (
        <>
        <NavBar />
        <SearchBar/>
        <div className="grid grid-cols-2 gap-4 mb-auto">
        <Cards/>
        <Cards/>
        <Cards/>
        <Cards/>
        </div>
        <Footer/>
        </>
    )
}
