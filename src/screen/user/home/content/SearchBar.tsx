import { Button, ButtonGroup, Input, InputGroup, InputLeftElement } from "@chakra-ui/react"
import { HiOutlineSearch } from "react-icons/hi";
import { useNavigate } from "react-router-dom";

const SearchBar = () => {
    const navigate = useNavigate();
    return (
        <>
            <div className="mt-10">
                <p className="text-2xl font-semibold">Hai Puki</p>
                <p className="text-lg text-secondary font-semibold">Mari kita cari pesanan Anda</p>
            </div>
            <InputGroup className="mt-10">
                <InputLeftElement pointerEvents='none'>
                    <HiOutlineSearch size={20} />
                </InputLeftElement>
                <Input type='text' placeholder='Cari kebutuhan mu!' borderRadius={15} boxShadow="md" />
            </InputGroup>
            <div className="flex mt-8 lg:mt-14 justify-center items-center">
                <ButtonGroup variant="solid" spacing={-2}  >
                    <Button className="w-[40vw]" size='lg' colorScheme='whatsapp' variant='solid' borderLeftRadius={20} onClick={() => navigate("/merchant")}>Barang</Button>
                    <Button className="w-[40vw] border-l-2 border-solid border-gray-200" size='lg' colorScheme='whatsapp' variant='solid' borderLeftRadius={0} borderRightRadius={20} onClick={() => navigate("/client")}>Jasa</Button>
                </ButtonGroup>
            </div>
        </>
    )
}

export default SearchBar