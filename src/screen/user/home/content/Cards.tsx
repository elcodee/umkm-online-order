import { Card, CardHeader, CardBody, Text } from '@chakra-ui/react'
import { useNavigate } from "react-router-dom";
const Cards = () => {
    const navigate = useNavigate();
    return (
        <>
            <Card
                borderRadius={20}
                className="relative grid h-[11rem] w-full max-w-[11rem] items-end justify-center overflow-hidden text-center mt-20 -mb-20"
                onClick={() => navigate("/client/item")}
            >
                <CardHeader
                    boxShadow="md"
                    color="transparent"
                    className="absolute inset-0 m-0 h-full w-full rounded-none bg-[url('https://i.ibb.co/P6FyxmJ/image.png')] bg-cover bg-center"
                >
                    
                </CardHeader>
                <CardBody className="m-auto py-14 px-6">
                <Text className='relative text-white text-3xl mt-10'>Tejo</Text>
                    <div className="flex items-stretch ">
                        <p className="text-white self-start absolute bottom-6 left-2 text-[10px]">Mulai Dari</p>
                        <p className="text-white self-start absolute bottom-2 left-2 text-[14px]">Rp.12000</p>
                    </div>
                </CardBody>
            </Card>
        </>
    );
}

export default Cards