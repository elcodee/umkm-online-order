import { useNavigate } from "react-router-dom";
import { Button, Card, CardBody, CardHeader } from "@chakra-ui/react"

export const Index = () => {
    const navigate = useNavigate();
    return (
        <>
            <div className="flex justify-center items-center -mt-[4.5rem]">
                <img src="/assets/images/bg-gradient.png" className='' alt="" />
            </div>

            <div className="flex justify-center items-center m-auto w-[24rem] -mt-[19rem]">
                <img src="/assets/images/user/items.png" className='' alt="" />
            </div>
            <div className="m-auto">
            <p className="text-xl font-semibold text-gray-900 mt-24 lg:mt-16 px-4">
                Mau pesan ini? <br />
                Ayo hubungi pedangang!
            </p>
            </div>
            <div className="flex justify-between mt-14">
                <div className="grid grid-rows-2 gap-6 w-48">
                    <div>
                        <h1 className="text-2xl font-bold text-[#3F3D56]">Sate</h1>
                        <h2 className="text-lg font-semibold text-[#3F3D56]">About this product</h2>
                    </div>
                    <p>Menyediakan Sate ayam dan kambing</p>
                </div>
                <Card
                borderRadius={20}
                className="relative grid h-[8rem] w-full max-w-[8rem] items-end justify-center overflow-hidden text-center"
            >
                <CardHeader
                    boxShadow="md"
                    color="transparent"
                    className="absolute inset-0 m-0 h-full w-full rounded-none bg-[url('https://i.ibb.co/P6FyxmJ/image.png')] bg-cover bg-center"
                >
                    
                </CardHeader>
                <CardBody className="m-auto py-14 px-6">
                </CardBody>
            </Card>
            </div>
            <div className="flex justify-center m-12 drop-shadow-2xl shadow-2xl">
                        <Button color="white" size='lg' w={300} bgColor={['#5DB329']} variant='solid' borderRadius={25} onClick={() => navigate("/client/home")}>Contact the seller</Button>
                    </div>
        </>
    );
}

export default Index