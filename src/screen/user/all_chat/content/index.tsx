import { Avatar, Box, Card, CardHeader, Flex, Heading, Input, InputGroup, InputLeftElement, Text } from '@chakra-ui/react'
import { HiOutlineSearch } from 'react-icons/hi'

export const Index = () => {
  return (
    <>
      <div>
        <InputGroup className="mb-8">
          <InputLeftElement pointerEvents='none'>
            <HiOutlineSearch size={20} />
          </InputLeftElement>
          <Input type='text' placeholder='Cari kebutuhan mu!' borderRadius={15} boxShadow="md" />
        </InputGroup>
      </div>
      <div className="-ml-9 lg:-ml-9 min-w-[26.5rem] lg:min-w-[24.5rem] grid grid-cols-1 lg:grid-cols-1">
        {
          [1, 2, 3].map(() => {
            return (
              <>
                <Card className=''>
                  <CardHeader>
                    <Flex>
                      <Flex flex='1' gap='4' alignItems='center' flexWrap='wrap'>
                        <Avatar name='Segun Adebayo' src='https://bit.ly/sage-adebayo' />

                        <Box>
                          <Heading size='sm'>Dildo</Heading>
                          <Text>Cepet mas pengen crt</Text>
                        </Box>
                      </Flex>
                          <Text ml={0}>14:17</Text>
                    </Flex>
                  </CardHeader>
                </Card>
              </>
            )
          })
        }
      </div>
    </>
  )
}

export default Index