import { useNavigate } from "react-router-dom";
import { Button, Input, InputGroup, InputLeftElement, InputRightElement, VStack } from "@chakra-ui/react"
import { useState } from "react";
import { HiOutlineEye, HiOutlineEyeOff, HiOutlineLockClosed, HiOutlineMail, HiOutlineUser } from "react-icons/hi";

export const Index = () => {
    const navigate = useNavigate();
    const [show, setShow] = useState(false)

    return (
        <>
            <div className="flex justify-center items-center -mt-[4rem]">
                <img src="/assets/images/bg-gradient.png" className='' alt="" />
            </div>

            <div className="flex justify-center items-center m-auto w-[20rem] -mt-[19rem]">
                <img src="/assets/images/user/signup-user.png" className='' alt="" />
            </div>

            <p className="text-lg text-center text-gray-900 mb-10 lg:mb-5 mt-24 lg:mt-32">
                Ayo daftar untuk memulai!
            </p>


            <div className="flex justify-center items-center">
                <VStack
                    spacing={4}
                >
                    <InputGroup>
                        <InputLeftElement pointerEvents='none'>
                            <HiOutlineMail size={24} />
                            {/* <PhoneIcon color='gray.300' /> */}
                        </InputLeftElement>
                        <Input type='email' placeholder='Email' />
                    </InputGroup>

                    <InputGroup>
                        <InputLeftElement pointerEvents='none'>
                            <HiOutlineUser size={24} />
                            {/* <PhoneIcon color='gray.300' /> */}
                        </InputLeftElement>
                        <Input type='text' placeholder='Nama Pengguna' />
                    </InputGroup>

                    <InputGroup>
                        <InputLeftElement pointerEvents='none'>
                            <HiOutlineLockClosed size={24} />
                            {/* <PhoneIcon color='gray.300' /> */}
                        </InputLeftElement>
                        <Input type={show ? 'text' : 'password'} placeholder='Kata Sandi' />
                        <InputRightElement width='4.5rem'>
                            <Button h='1.75rem' size='sm' onClick={() => setShow(!show)}>
                                {show ? <HiOutlineEyeOff size={20} /> : <HiOutlineEye size={20} />}
                            </Button>
                        </InputRightElement>
                    </InputGroup>

                    <div className="mt-10">
                        <Button color="white" size='lg' w={300} bgColor={['#5DB329']} variant='solid' borderRadius={20} onClick={() => navigate("/client/success")}>Daftar</Button>
                    </div>
                </VStack>
            </div>
        </>
    )
}
