import { Card, CardHeader, CardBody, Text } from '@chakra-ui/react'
import { useNavigate } from "react-router-dom";
const Cards = () => {
    const navigate = useNavigate();
    return (
        <>
            <Card
                borderRadius={20}
                className="relative grid h-[11rem] w-full max-w-[11rem] items-end justify-center overflow-hidden text-center mt-20 -mb-20"
                onClick={() => navigate("/client/sign-up")}
            >
                <CardHeader
                    boxShadow="md"
                    color="transparent"
                    className="absolute inset-0 m-0 h-full w-full rounded-none bg-[url('https://i.ibb.co/P6FyxmJ/image.png')] bg-cover bg-center"
                >
                    
                </CardHeader>
                <CardBody className="m-auto py-14 px-6">
                <Text className='relative text-white text-3xl mt-10 font-extrabold'>Tejo</Text>
                    <div className="flex items-stretch ">
                        <p className="text-white font-semibold self-start absolute bottom-2 left-3 text-[10px]">Hubungi Tejo</p>
                    </div>
                </CardBody>
            </Card>
        </>
    );
}

export default Cards