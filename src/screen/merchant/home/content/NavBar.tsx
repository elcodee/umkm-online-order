import { useNavigate } from "react-router-dom";
import { Box, IconButton, Avatar, Drawer, DrawerOverlay, DrawerContent, DrawerCloseButton, DrawerHeader, DrawerBody, DrawerFooter, useDisclosure } from "@chakra-ui/react"
import { HiMenu, HiLocationMarker, HiOutlineUser, HiOutlineSpeakerphone, HiOutlineReply } from "react-icons/hi";

export const NavBar = () => {
    const navigate = useNavigate();
    const { isOpen, onOpen, onClose } = useDisclosure();
    return (
        <>
            <div className="">
                <Box bg='#5DB329' p={8} color='white' className="-ml-10 -mr-10 -mt-9 flex justify-between items-center max-h-[0px]">
                    <IconButton
                        colorScheme='transparant'
                        aria-label='Search database'
                        icon={<HiMenu className="size-8 m-auto" />}
                        onClick={onOpen}
                    />
                    <Drawer
                        isOpen={isOpen}
                        placement='left'
                        onClose={onClose}
                    >
                        <DrawerOverlay />
                        <DrawerContent>
                            <DrawerCloseButton />
                            <DrawerHeader>
                                <div className="flex justify-normal">
                                    <Avatar size='lg' name='Dan Abrahmov' src='https://bit.ly/dan-abramov' />
                                    <div className="ml-4 grid grid-rows-2">
                                        <h1 className="text-2xl font-semibold">Andre</h1>
                                        <p className="text-base">mail@mail.com</p>
                                    </div>
                                </div>
                            </DrawerHeader>

                            <DrawerBody>
                                <div className="grid grid-rows-2 gap-1 mt-64">
                                    <div className="flex justify-normal items-center"
                                    onClick={() => navigate("/client/profile")}>
                                        <HiOutlineUser className="size-6 mr-3" />
                                        <p className="text-2xl font-semibold">Profil</p>
                                    </div>
                                    <div className="flex justify-normal items-center">
                                        <HiOutlineSpeakerphone
                                            className="size-6 mr-3" />
                                        <p className="text-2xl font-semibold">Laporkan</p>
                                    </div>
                                    <div className="flex justify-normal items-center mt-72"
                                    onClick={() => navigate("/")}>
                                        <HiOutlineReply
                                            className="size-6 mr-3" />
                                        <p className="text-2xl ">Keluarkan Akun</p>
                                    </div>
                                </div>
                            </DrawerBody>

                            <DrawerFooter>
                            </DrawerFooter>
                        </DrawerContent>
                    </Drawer>
                    <div className="flex gap-1 -ml-3">
                        <HiLocationMarker className="size-6 m-auto" />
                        <p className="text-1xl font-semibold m-auto">Bandung</p>
                    </div>
                    <Avatar size="sm" name='Dan Abrahmov' src='https://bit.ly/dan-abramov' onClick={() => navigate("/merchant/profile")} />
                </Box>
            </div>
        </>
    )
}

export default NavBar