import { useNavigate } from "react-router-dom";
import { Button, Image, VStack, Input,  FormControl,  FormLabel } from "@chakra-ui/react"

export const Index = () => {
    const navigate = useNavigate();
    return (
        <>
            <div className="flex justify-center items-center">
                <Image
                    boxSize='100px'
                    objectFit='cover'
                    src='https://bit.ly/dan-abramov'
                    alt='Dan Abramov'
                    borderRadius={10}
                />
            </div>
            <VStack spacing={4} className="mt-16">
                <FormControl>
                    <FormLabel>Nama Pengguna</FormLabel>
                    <Input type='text' borderRadius={15} boxShadow='md'/>
                </FormControl>
                <FormControl>
                    <FormLabel>Password</FormLabel>
                    <Input type='password'borderRadius={15} boxShadow='md'/>
                </FormControl>
                <FormControl>
                    <FormLabel>Nomor Telefon</FormLabel>
                    <Input type='text' borderRadius={15} boxShadow='md'/>
                </FormControl>
                <FormControl>
                    <FormLabel>Mulai Dari</FormLabel>
                    <Input type='text' borderRadius={15} boxShadow='md'/>
                </FormControl>
                <FormControl>
                    <FormLabel>Deskripsi</FormLabel>
                    <Input type='text' borderRadius={15} boxShadow='md' height="20"/>
                </FormControl>
                <div className="mt-14">
                    <Button color="white" size='lg' w={300} bgColor={['#5DB329']} variant='solid' borderRadius={20} onClick={() => navigate("/merchant")}>Simpan Perubahan</Button>
                </div>
            </VStack>
        </>
    )
}

export default Index